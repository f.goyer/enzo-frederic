#ifndef ALBUM_H
#define ALBUM_H
#include <string>
#include <chrono>

/**
 * \class Album
 * \brief Definition of the Album class
 */
class Album
{
 public:

  /**
   * \brief Default Constructor of Album
   */
  Album();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of an album
   * \param[in] _date release date of an album
   * \param[in] _name name of an album
   */
  Album(unsigned int _id, std::chrono::system_clock _date, std::string _name);

  /**
   * \brief Lets you know the ID of an album
   * \return unsigned int represents the ID of an album
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the release date of an album
   * \return std::chrono::system_clock represents the release date of an album
   */
  std::chrono::system_clock get_date();

  /**
   * \brief Lets you know the name of an album
   * \return std::string represents the name of an album
   */
  std::string get_name();

 private:
  unsigned int id; ///< Attribute defining the ID of an album
  std::chrono::system_clock date; ///< Attribute determining the release date of an album
  std::string name; ///< Attribute determining the name of an album
};
#endif
