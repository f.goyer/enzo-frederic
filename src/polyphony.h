#ifndef POLYPHONY_H
#define POLYPHONY_H
#include <string>

/**
 * \class Polyphony
 * \brief Definition of the Polyphony class
 */
class Polyphony
{
 public:

  /**
   * \brief Default constructor of Polyphony
   */
  Polyphony();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of a polyphony
   * \param[in] _number the number of canal of a polyphony
   */
  Polyphony(unsigned int _id, unsigned short int _number);

  /**
   * \brief Lets you know the id of a polyphony
   * \return unsigned int representing the id of a polyphony
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the number of canal of a polyphony
   * \return unsigned short int representing the number of canal of a polyphony
   */
  unsigned short int get_number();

 private:
  unsigned int id; ///< Attribute defining the id of the polyphony
  unsigned short int number; ///< Attribute defining the number of canal of the polyphony
};
#endif
