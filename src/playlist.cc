#include <iostream>
#include <fstream>
#include <cstring>
#include <libpq-fe.h>
#include "playlist.h"
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>

Playlist::Playlist() :
  id(0),
  title("playlist"),
  duration(0),
  tracks()
{}

Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track> _tracks) :

  id(_id),
  title(_title),
  duration(_duration),
  tracks(_tracks)
{}

void Playlist::set_duration(std::chrono::duration<int> _duration)
{
  duration = _duration;
}

void Playlist::set_track(std::vector<Track> _tracks)
{
  tracks = _tracks;
}

unsigned int Playlist::get_id()
{
  return id;
}

std::string Playlist::get_title()
{
  return title;
}
std::chrono::duration<int> Playlist::get_duration()
{
  return duration;
}

std::vector<Track> Playlist::get_track()
{
  return tracks;
}


void Playlist::writeXSPF()

{
  int code_retour = 0;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=e.casciano password=P@ssword";
  PGPing code_retour_ping = PQping(info_connexion);
  PGconn *connexion;
  Xspf::XspfIndentFormatter formatter;
  char *reponse_requete;
  connexion = PQconnectdb(info_connexion);
  const char requete_sql[] = "SET SCHEMA 'radio_libre'; select * from \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id inner join \"album_morceau\" on \"morceau\".id = \"album_morceau\".id_morceau inner join \"album\" on \"album_morceau\".id_album = \"album\".id";
  PGresult *resultat = PQexec(connexion, requete_sql);
  XML_Char const *const baseUri = _PT("http://radio6mic.net/");
  Xspf::XspfWriter *const writer = Xspf::XspfWriter::makeWriter(formatter, baseUri);
  Xspf::XspfTrack *track;
  srand(time(NULL));

  for(int i = 0; i < 10; i++)
  {
    int j = rand() % PQntuples(resultat);
    track = new Xspf::XspfTrack();
    char *name = PQgetvalue(resultat, j, 2);
    char *path = PQgetvalue(resultat, j, 3);
    char *artist = PQgetvalue(resultat, j, 11);
    char *album = PQgetvalue(resultat, j, 16);
    track->lendTitle(_PT(name));
    track->lendAppendLocation(_PT(path));
    track->lendCreator(_PT(artist));
    track->lendAlbum(_PT(album));
    writer->addTrack(track);
  }

  writer->writeFile(_PT("playlist.xspf"));
}

void Playlist::writeM3U()
{
  int code_retour = 0;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=e.casciano password=P@ssword";
  PGPing code_retour_ping = PQping(info_connexion);
  PGconn *connexion;
  Xspf::XspfIndentFormatter formatter;
  char *reponse_requete;
  connexion = PQconnectdb(info_connexion);
  const char requete_sql[] = "SET SCHEMA 'radio_libre'; select * from \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id inner join \"album_morceau\" on \"morceau\".id = \"album_morceau\".id_morceau inner join \"album\" on \"album_morceau\".id_album = \"album\".id";
  PGresult *resultat = PQexec(connexion, requete_sql);
  std::string name = title + ".m3u";
  std::ofstream m3u_file;
  m3u_file.open(name, std::ios::out);
  srand(time(NULL));

  for(int i = 1; i <= 10; i++)
  {
    int j = rand() % PQntuples(resultat);
    m3u_file.write(PQgetvalue(resultat, j, 3), strlen(PQgetvalue(resultat, j, 3)));
    m3u_file.write("\n\n", strlen("\n\n"));
  }

  m3u_file.close();
}
