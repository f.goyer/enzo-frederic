#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <string>
#include <chrono>
#include <vector>
#include "track.h"
#include <libpq-fe.h>
#include <iostream>
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>
#include <stdlib.h>
#include <time.h>

/**
 * \class Playlist
 * \brief Definition of the Playlist class
 */
class Playlist
{
 public:
  /**
   * \brief Default constructor of Playlist
   */
  Playlist();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of a playlist
   * \param[in] _title the title of a playlist
   * \param[in] _duration the duration of a playlist
   * \param[in] _tracks Tracks in the playlist
   */
  Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track> _tracks);

  /**
   * \brief Lets you know the id of a playlist
   * \return unsigned int representing the id of a playlist
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the title of a playist
   * \return std::string representing the title of a playlist
   */
  std::string get_title();

  /**
   * \brief Lets you know the duration of a playlist
   * \return std::chrono::duration<int> representing the duration of a playlist
   */
  std::chrono::duration<int> get_duration();

  /**
   * \brief Lets you know a track of a playlist
   * \return std::vector<Track> representing a track of a playlist
   */
  std::vector<Track> get_track();


  /**
   * \brief Lets you change the duration of the playlist
   * \return std::chrono::duration<int> representing the duration of a playlist
   */
  void set_duration(std::chrono::duration<int> _duration);

  /**
   * \brief Lets you change tracks of the playlist
   * \return std::vector<Track> representing tracks of a playlist
   */
  void set_track(std::vector<Track> _tracks);

  void writeM3U();
  void writeXSPF();

 private:
  unsigned int id; ///< Attribute defining the id of the playlist
  std::string title; ///< Attribute defining the title of the playlist
  std::chrono::duration<int> duration; ///< Attribute defining the duration of the playlist
  std::vector<Track> tracks; ///< Attribute defining tracks of the playlist
};
#endif
