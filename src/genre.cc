#include <iostream>
#include "genre.h"

Genre::Genre() :
  id(0),
  type("")
{}

Genre::Genre(unsigned int _id,
             std::string _type) :
  id(_id),
  type(_type)
{}

unsigned int Genre::get_id()
{
  return id;
}

std::string Genre::get_type()
{
  return type;
}

