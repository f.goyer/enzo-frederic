@startuml

class Playlist {
  -id: unsigned int
  -duration: std::chrono::duration<int>
  -title: std::string


  +Playlist()
  +Playlist(unsigned int, std::chrono::duration<int>)
  +get_id(): unsigned int
  +get_duration(): std::chrono::duration
  +get_title(): std::string
  +set_duration(std::chrono::duration duration): void
}

class Track {
  -id: unsigned int
  -duration: std::chrono::duration<int>
  -name: std::string
  -path: std::string
  -artists: std::vector<Artist>
  -albums: std::vector<Album>
  -a_polyphony: Polyphony
  -a_format: Format
  -a_subgenre: SubGenre
  -a_genre: Genre

  +Track()
    +Track(unsigned int, std::chrono::duration<int>, std::string, std::string, std::vector<Artist>, std::vector<Album>, Polyphony, Format, SubGenre, Genre)
  +get_id(): unsigned int
  +get_duration(): std::chrono::duration
  +get_name(): std::string
  +get_path(): std::string
  +get_artists(): std::vector<Artist>
  +get_albums(): std::vector<Album>
  +get_polyphony(): Polyphony
  +get_format(): Format
  +get_subgenre(): SubGenre
  +get_genre(): Genre
}

class Artist {
  -id: unsigned int
  -name: std::string

  +Artist()
  +Artist(unsigned int, std::string)
  +get_id(): unsigned int
  +get_name(): std::string
}

class Album {
  -id: unsigned int
  -date: std::chrono::system_clock
  -name: std::string

  +Album()
  +Album(unsigned int, std::chrono::system_clock, std::string)
  +get_id(): unsigned int
  +get_date(): std::chrono::system_clock
  +get_name(): std::string
}

class Format {
  -id: unsigned int
  -titule: std::string

  +Format()
  +Format(unsigned int, std::string)
  +get_id(): unsigned int
  +get_titule(): std::string
  +set_titule(std::string): void
}

class SubGenre {
  -id: unsigned int
  -sub_type: std::string

  +SubGenre()
  +SubGenre(unsigned int, std::string)
  +get_id(): unsigned int
  +get_sub_type(): std::string
}

class Genre {
  -id: unsigned int
  -type: std::string

  +Genre()
  +Genre(unsigned int, std::string)
  +get_id(): unsigned int
  +get_type(): std::string
}

class Polyphony {
  -id: unsigned int
  -number: unsigned short int

  +Polyphony()
  +Polyphony(unsigned int, unsigned short int)
  +get_id(): unsigned int
  +get_number(): unsigned short int
}

class Cli {
}

Playlist "0..*" o-- "1..*" Track
Track "1..*" -- "1..*" Artist : -artists
Track "1..*" -- "1..*" Album : -albums
Track "1..*" -- "1" Format : -format
Track "1..*" -- "1" SubGenre : -subgenre
Track "1..*" -- "1" Genre : -genre
Track "1..*" -- "1..*" Polyphony : -polyphonys
@enduml
